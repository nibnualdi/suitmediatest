// Navbar

const nav = document.querySelector('.nav')

const dropdownAbout = document.querySelector('.dropdown-about')

nav.addEventListener('mouseover', (e)=>{
	if (e.target.innerHTML === 'ABOUT' || e.target.innerHTML === 'HISTORY' || e.target.innerHTML === 'VISION MISSION') {
		dropdownAbout.style.display = 'block'
	}
})


nav.addEventListener('mouseout', (e)=>{
		dropdownAbout.style.display = 'none'
})




// Slider

const slider = document.querySelector('.slider')
const sliderImage = document.querySelectorAll('.slider .img')

// Buttons
const nextBtn = document.querySelector('#nextBtn')
const prevBtn = document.querySelector('#prevBtn')

// Bubbles
const bubbles = document.querySelectorAll('.bubble')

// Program
let counter = 1
const sliderSize = sliderImage[0].clientWidth
let firstbubble = true

slider.style.transform = `translateX(${(-sliderSize * counter)}px)`
bubbles[0].style.opacity = '1'


// Button Listeners

nextBtn.addEventListener('click', ()=>{
	// If more than image slider, then do nothing
	if(counter >= sliderImage.length - 1 ) return

	// else
	slider.style.transition = 'transform 0.4s ease-in-out'
	counter++
	slider.style.transform = `translateX(${(-sliderSize * counter)}px)`


	if (firstbubble === true) {
		firstbubble = false
		bubbles[0].style.opacity = '.3'
		bubbles[1].style.opacity = '1'
	} else {
		firstbubble = true
		bubbles[0].style.opacity = '1'
		bubbles[1].style.opacity = '.3'
	}

})

prevBtn.addEventListener('click', ()=>{
	// If more than image slider, then do nothing
	if(counter <= 0 ) return

	// else
	slider.style.transition = 'transform 0.4s ease-in-out'
	counter--
	slider.style.transform = `translateX(${(-sliderSize * counter)}px)`

	if (firstbubble === true) {
		firstbubble = false
		bubbles[0].style.opacity = '.3'
		bubbles[1].style.opacity = '1'
	} else {
		firstbubble = true
		bubbles[0].style.opacity = '1'
		bubbles[1].style.opacity = '.3'
	}
	
})

slider.addEventListener('transitionend', ()=>{
	
	if(sliderImage[counter].id === 'firstCopied') {
		slider.style.transition = 'none'
		counter = 1
		slider.style.transform = `translateX(${(-sliderSize * counter)}px)`
	}

	if(sliderImage[counter].id === 'lastCopied') {
		slider.style.transition = 'none'
		counter = sliderImage.length - 2
		slider.style.transform = `translateX(${(-sliderSize * counter)}px)`
	}
})